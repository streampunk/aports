# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze
pkgver=5.23.2
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
# mips64, s390x and riscv64 blocked by polkit -> kconfigwidgets
arch="all !armhf !s390x !mips64 !riscv64"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends_dev="
	kconfigwidgets-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kpackage-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
ba93d190fa96764a8ec1ade0260386f639882e6726fdf665d68aae982f726ff18decd86af5434815ea10426cdefd31cd7fd165b8e8a795b269da47b0771b01cb  breeze-5.23.2.tar.xz
"
