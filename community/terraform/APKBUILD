# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.0.10
pkgrel=1
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/github.com/hashicorp/$pkgname"
options="chmod-clean"

prepare() {
	mkdir -p ${builddir%/*}
	mv $srcdir/$pkgname-$pkgver "$builddir"/
	default_prepare
}

export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-mod=readonly -ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease= -s -w"
}

check() {
	case "$CARCH" in
		arm*|x86)
			go list -mod=readonly . | xargs -t -n4 \
			go test -mod=readonly -timeout=2m -parallel=4
			;;
		*) go test ./... ;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
7a32ade193427959ae9a1c7a3a7bc8dcaf6ae3a8b91388e68ad9fbb151df7ee9e0278693cfb54ebf7f1aaeece46ff4094451f4aabb1912a09f2a9468565cb050  terraform-1.0.10.tar.gz
"
