# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=bctoolbox
pkgver=5.0.46
pkgrel=0
pkgdesc="Utilities library used by Belledonne Communications softwares like belle-sip, mediastreamer2 and linphone"
url="https://github.com/BelledonneCommunications/bctoolbox"
arch="all"
license="GPL-2.0-or-later"
options="!check" # bcunit not available
makedepends="cmake mbedtls-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/BelledonneCommunications/bctoolbox/archive/$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_MBEDTLS=YES \
		-DENABLE_POLARSSL=NO \
		-DENABLE_STATIC=NO \
		-DENABLE_TESTS_COMPONENT=OFF \
		-DENABLE_SHARED=YES .
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/bctoolbox
	mv "$pkgdir"/usr/share/bctoolbox/cmake/* "$subpkgdir"/usr/lib/cmake/bctoolbox
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/bctoolbox/cmake
	rmdir "$pkgdir"/usr/share/bctoolbox
	rmdir "$pkgdir"/usr/share
}

sha512sums="
bd906753680edd4b5d13f666b8f979b9f33472b8b35ef20024f9f9fb5325ea827454a05f48feaeb4aafe31b7478a91d871d72d228fa2e7e2964d238ed4c84bb7  bctoolbox-5.0.46.tar.gz
"
