# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=mkinitfs
pkgver=3.5.0
# shellcheck disable=SC2034 # used for git versions, keep around for next time
_ver=${pkgver%_git*}
pkgrel=2
pkgdesc="Tool to generate initramfs images for Alpine"
url="https://gitlab.alpinelinux.org/alpine/mkinitfs"
arch="all"
license="GPL-2.0-only"
# currently we do not ship any testsuite
options="!check"
makedepends_host="busybox kmod-dev util-linux-dev cryptsetup-dev linux-headers"
makedepends="$makedepends_host"
depends="busybox>=1.28.2-r1 apk-tools>=2.9.1 lddtree>=1.25 kmod"
subpackages="$pkgname-doc"
install="$pkgname.pre-upgrade $pkgname.post-install $pkgname.post-upgrade"
triggers="$pkgname.trigger=/usr/share/kernel/*"
source="https://gitlab.alpinelinux.org/alpine/mkinitfs/-/archive/$pkgver/mkinitfs-$pkgver.tar.gz
	0001-Add-support-for-compressed-kernel-modules.patch
	"

build() {
	make VERSION=$pkgver-r$pkgrel
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="
27199775b26b9235306c026877c8be97c05ee3f6670fbb49a9da6f5a82a918a523b54b68414f0c5b73b9713ef20495cf5110081151f126817fc3351c34413928  mkinitfs-3.5.0.tar.gz
4478c58678c3ea853ddc8a861bd3c578e1dfaf247eb754eee5f72de67274c503b704b19d5a7b739ce4495084f8bf7ce74505c3994e05caa2d0e41e4c9a79628a  0001-Add-support-for-compressed-kernel-modules.patch
"
